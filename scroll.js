
$('#main').on('click', 'p.cite a', function(jq){
	var postId = $(this).prop('href').split('post')[1];

	var post = $('#post' + postId);

	if( post.length ){
		jq.preventDefault();
		jq.stopPropagation();

		$.scrollTo( post, 800 );
	}
});
